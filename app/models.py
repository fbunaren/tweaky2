from django.db import models

#Status model
class Status(models.Model):
    name = models.CharField(max_length=30)
    msg = models.TextField(max_length=1000)