from django.test import TestCase, LiveServerTestCase
from django.test import Client
import unittest
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time
from django.apps import apps
from django.http import HttpRequest

from .models import Status

# Create your tests here.
class AppTest(TestCase):
    def test_index_response(self):
        response = Client().get('/index.html')
        self.assertEqual(response.status_code,200)
    
    def test_index_template(self):
        response = Client().get('/index.html')
        self.assertTemplateUsed(response,'index.html')

    def test_status_add(self):
        #Making new Status
        status = Status.objects.create(msg="Test Status")

        #Check if status has been stored in database
        count_status = Status.objects.all().count()
        self.assertTrue(count_status > 0)

    def test_can_save_post_request(self):
        response = self.client.post('',data={'name':'Fransiscus','msg':'Tweaky'})

        #Check if status has been stored in database
        count_status = Status.objects.all().count()
        self.assertTrue(count_status > 0)
        
        #Check if webpage is accessible
        self.assertEqual(response.status_code,200)

        #Check if the content is the same
        new_response = self.client.get('')
        html_response = new_response.content.decode('utf8')
        self.assertIn('Tweaky',html_response)
    
    def test_confirm_page_response(self):
        response = Client().get('/confirm.html')
        self.assertEqual(response.status_code,200)

    def test_confirm_page_template(self):
        response = Client().get('/confirm.html')
        self.assertTemplateUsed(response,'confirm.html')

class FunctionalTestTweaky(LiveServerTestCase):
    link = 'https://tweaky.herokuapp.com/'

    def setUp(self):
        super().setUp()
        opt = webdriver.ChromeOptions()
        opt.add_argument('--no-sandbox')
        opt.add_argument('--headless')
        opt.add_argument('--disable-gpu')
        opt.add_argument('--disable-dev-shm-usage')
        self.selenium = webdriver.Chrome(chrome_options=opt,executable_path='chromedriver')

    def tearDown(self):
        self.selenium.refresh()
        self.selenium.quit()
        super().tearDown()

    def test_index_loaded(self):
        self.selenium.get(self.link)
        #Test if the H1 Header is Loaded
        self.assertEquals(self.selenium.find_element_by_tag_name('h1').text,'Tweaky')
        #Test if the form can be seen
        self.assertTrue(self.selenium.find_element_by_tag_name('form'))

    def test_form_yes(self):
        self.selenium.get(self.link)
        name = self.selenium.find_elements_by_name('name')[0]
        status = self.selenium.find_elements_by_name('msg')[0]
        submit = self.selenium.find_element_by_id('submit')

        name.send_keys('Fransiscus')
        status.send_keys('Tweaky')
        time.sleep(2)
        submit.click()

        time.sleep(2)
        try:
            self.selenium.find_element_by_id('yes_btn').click()
        except:
            pass

        time.sleep(2)
        self.assertIn('Fransiscus', self.selenium.page_source)
        self.assertIn('Tweaky', self.selenium.page_source)

    def test_form_no(self):
        self.selenium.get(self.link)
        name = self.selenium.find_elements_by_name('name')[0]
        status = self.selenium.find_elements_by_name('msg')[0]
        submit = self.selenium.find_element_by_id('submit')

        name.send_keys('Testeer')
        status.send_keys('Tweakyss')
        time.sleep(2)
        submit.click()

        time.sleep(2)
        try:
            self.selenium.find_element_by_id('no_btn').click()
        except:
            pass
        time.sleep(2)

        self.assertNotIn('Tester', self.selenium.page_source)
        self.assertNotIn('Tweakysss', self.selenium.page_source)