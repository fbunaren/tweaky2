from django.shortcuts import render
from .models import Status
from .forms import StatusForm

def index(request):
    response = {'form' : StatusForm()}
    if request.method == 'POST':
        status = Status.objects.create(name=request.POST['name'],msg=request.POST['msg'])
        status.save()
    response['statuses'] = Status.objects.all().values()
    return render(request,'index.html',response)

def confirm(request):
    try:
        response = {'name' : request.POST['name'] or "", 'msg' : request.POST['msg'] or ""}
    except:
        response = {'name' : "", 'msg' : ""}
    return render(request,'confirm.html',response)
    