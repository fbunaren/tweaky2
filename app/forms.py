from django import forms

class StatusForm(forms.Form):
    name = forms.CharField(max_length=30,widget=forms.TextInput(attrs={'style' : 'background-color: lightgray;border: 1px solid;font-size: 18;margin-bottom:10px;'}))
    msg = forms.CharField(label='',max_length=1000,required=True,widget=forms.Textarea(attrs={'style' : 'width: 100%;background-color: lightgray;border: 1px solid;font-size: 18;'}) )